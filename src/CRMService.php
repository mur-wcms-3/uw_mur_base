<?php

namespace Drupal\uw_mur_base;

/**
 * A CRM Service.
 */
class CRMService {
  /**
   * CRM URL.
   *
   * @var url
   */
  protected $url;

  /**
   * CRM Password.
   *
   * @var username
   */
  protected $username;

  /**
   * CRM Password.
   *
   * @var password
   */
  protected $password;

  /**
   * State manager.
   *
   * @var state
   */
  private $state;

  /**
   * Http client.
   *
   * @var httpClient
   */
  private $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct($state, $httpClient) {
    $this->url = $state->get('uw_mur_base.crm_host');
    $this->username = $state->get('uw_mur_base.crm_username');
    $this->password = $state->get('uw_mur_base.crm_password');
    $this->state = $state;
    $this->httpClient = $httpClient;
  }

  /**
   * Call CRM API Endpoint.
   *
   * @param string $method
   *   CRM Method to call (i.e., login, set_entry, etc.).
   * @param array $parameters
   *   Array of values to upload to the CRM as defined by its API.
   */
  public function call($method, array $parameters) {
    $client = $this->httpClient;
    $data = json_encode($parameters);

    $response = $client->request('POST', $this->url, [
      'form_params' => [
        "method" => $method,
        "input_type" => "JSON",
        "response_type" => "JSON",
        "rest_data" => $data,
      ],
      'verify' => FALSE,
    ]);

    return json_decode($response->getBody(), TRUE);
  }

  /**
   * Get CRM Session ID.
   *
   * @returns string Logs into the CRM and returns a session ID.
   */
  public function getSession() {
    $login_parameters = [
      "user_auth" => [
        "user_name" => $this->state->get('uw_mur_base.crm_username'),
        "password" => $this->state->get('uw_mur_base.crm_password'),
        "version" => "1",
      ],
      "application_name" => "brochure-request",
      "name_value_list" => [],
    ];

    $response = $this->call('login', $login_parameters);

    return $response['id'];
  }

}
