<?php

namespace Drupal\uw_mur_base\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Settings Form.
 */
class SettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->config = $instance->configFactory->getEditable('uw_mur_base.settings');
    $instance->state = $container->get('state');
    $instance->messenger = $container->get('messenger');
    $instance->module_handler = $container->get('module_handler');
    $instance->module_installer = $container->get('module_installer');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uw_mur_base_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Vertical tabs.
    $form['tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'crm',
    ];

    // CRM Settings.
    $form['crm'] = [
      '#type' => 'details',
      '#title' => $this->t('CRM Settings'),
      '#group' => 'tabs',
    ];

    $crm_host = $this->state->get('uw_mur_base.crm_host');
    $form['crm']['crm_host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CRM Host URL'),
      '#default_value' => $crm_host,
    ];

    $crm_username = $this->state->get('uw_mur_base.crm_username');
    $form['crm']['crm_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CRM Username'),
      '#default_value' => $crm_username,
    ];

    $form['crm']['change_password'] = [
      '#markup' => '<br><h3>Change CRM Password</h3><hr />',
    ];
    $form['crm']['crm_password'] = [
      '#type' => 'password_confirm',
    ];

    $crm_password = !empty($this->state->get('uw_mur_base.crm_password'));
    $set = $crm_password ? 'set' : 'not set';
    $form['crm']['crm_password_label'] = [
      '#markup' => "<b>Password is currently <u>$set</u>.</b><br><br>" ,
    ];

    if ($crm_password) {
      $form['crm']['clear_password'] = [
        '#type' => 'submit',
        '#value' => $this->t('Clear Password'),
        '#submit' => ['::clearPassword'],
      ];
    }

    $form['crm']['entry_year_label'] = [
      '#markup' => '<br><br><h3>Entry Year Field Settings</h3><hr />',
    ];
    // Entry Year Rollover Month.
    $crm_entry_year_rollover_month = $this->state->get('uw_mur_base.crm_entry_year_rollover_month');
    $form['crm']['crm_entry_year_rollover_month'] = [
      '#type' => 'select',
      '#title' => $this->t('Rollover Month'),
      '#description' => $this->t('Specify the month when <b>Entry Year</b> dropdown should hide the current year and only display future year options.'),
      '#default_value' => $crm_entry_year_rollover_month ?: '7',
      '#required' => TRUE,
      '#options' => [
        '1' => $this->t('January'),
        '2' => $this->t('February'),
        '3' => $this->t('March'),
        '4' => $this->t('April'),
        '5' => $this->t('May'),
        '6' => $this->t('June'),
        '7' => $this->t('July'),
        '8' => $this->t('August'),
        '9' => $this->t('September'),
        '10' => $this->t('October'),
        '11' => $this->t('November'),
        '12' => $this->t('December'),
      ],
    ];
    // Entry Year Rollover Day.
    $crm_entry_year_rollover_day = $this->state->get('uw_mur_base.crm_entry_year_rollover_day');
    $form['crm']['crm_entry_year_rollover_day'] = [
      '#type' => 'select',
      '#title' => $this->t('Rollover Day'),
      '#description' => $this->t('Specify the day of the month when <b>Entry Year</b> dropdown should hide the current year and only display future year options.'),
      '#default_value' => $crm_entry_year_rollover_day ?: '1',
      '#required' => TRUE,
      '#options' => array_combine(range(1, 31), range(1, 31)),
    ];

    // Other Settings.
    $form['other'] = [
      '#type' => 'details',
      '#title' => $this->t('Other Settings'),
      '#group' => 'tabs',
    ];

    $form['other']['note'] = [
      '#markup' => "
        <h2>Module Installers</h2>
        <p>This section allows you to enable MUR custom modules.</p>
        <hr>
        <h4>Admission Requirements</h4>",
    ];

    // Install Action.
    $form['other']['install_admission_requirements'] = [
      '#type' => 'submit',
      '#value' => $this->t('Install Admission Requirements Module'),
      '#submit' => ['::installAdmissionRequirements'],
      '#disabled' => $this->module_handler->moduleExists('uw_admission_requirements'),
    ];

    if ($this->module_handler->moduleExists('uw_admission_requirements')) {
      $form['other']['note2'] = [
        '#markup' => "<p>Module is currenty installed</p>",
      ];
    }

    $form['other']['note3'] = [
      '#markup' => "<br><br><hr><h4>Brochure Request</h4>",
    ];

    // Install Action.
    $form['other']['install_uw_brochure_request'] = [
      '#type' => 'submit',
      '#value' => $this->t('Install Brochure Request Module'),
      '#submit' => ['::installBrochureRequest'],
      '#disabled' => $this->module_handler->moduleExists('uw_brochure_request'),
    ];

    if ($this->module_handler->moduleExists('uw_brochure_request')) {
      $form['other']['note4'] = [
        '#markup' => "<p>Module is currenty installed</p>",
      ];
    }

    $form['other']['note5'] = [
      '#markup' => "<br><br><hr><h4>Budget Calculator</h4>",
    ];

    // Install Action.
    $form['other']['install_uw_budget_calculator'] = [
      '#type' => 'submit',
      '#value' => $this->t('Install Budget Calculator Module'),
      '#submit' => ['::installBudgetCalculator'],
      '#disabled' => $this->module_handler->moduleExists('uw_budget_calculator'),
    ];

    if ($this->module_handler->moduleExists('uw_budget_calculator')) {
      $form['other']['note6'] = [
        '#markup' => "<p>Module is currenty installed</p>",
      ];
    }

    $form['other']['note7'] = [
      '#markup' => "
        <br><br>
        <h2>Cache Settings</h2>
        <hr><br>",
    ];

    // Clear cache button.
    $form['other']['clear_cache'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clear Cache'),
      '#submit' => ['::clearCache'],
    ];

    // Submit Action.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Changes'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $crm_host = $form_state->getValue('crm_host');
    $this->state->set('uw_mur_base.crm_host', $crm_host);

    $crm_username = $form_state->getValue('crm_username');
    $this->state->set('uw_mur_base.crm_username', $crm_username);

    $crm_password = $form_state->getValue('crm_password');
    if (!empty($crm_password)) {
      $this->state->set('uw_mur_base.crm_password', md5($crm_password));
    }

    $crm_entry_year_rollover_month = $form_state->getValue('crm_entry_year_rollover_month');
    $this->state->set('uw_mur_base.crm_entry_year_rollover_month', $crm_entry_year_rollover_month);

    $crm_entry_year_rollover_day = $form_state->getValue('crm_entry_year_rollover_day');
    $this->state->set('uw_mur_base.crm_entry_year_rollover_day', $crm_entry_year_rollover_day);

    // Save our setting changes.
    $this->config->save();

    $this->messenger->addStatus('Settings have been saved');
  }

  /**
   * Clears the saved CRM password.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state array.
   */
  public function clearPassword(array &$form, FormStateInterface $form_state) {

    $this->state->delete('uw_mur_base.crm_password');

    // Save our setting changes.
    $this->config->save();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('The CRM password has been cleared.'));
  }

  /**
   * Installs Brochure Request module.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state array.
   */
  public function installBrochureRequest(array &$form, FormStateInterface $form_state) {
    // Remove configuration data.
    $this->configFactory->getEditable('layout_builder_browser.layout_builder_browser_block.brochure_request')->delete();
    $this->configFactory->getEditable('layout_builder_browser.layout_builder_browser_block.brochure_bulk_request')->delete();

    $this->module_installer->install(['uw_brochure_request']);

    // Display a message on completion.
    $this->messenger->addStatus($this->t('The Brochure Request module has been installed.'));
  }

  /**
   * Installs Budget Calculator module.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state array.
   */
  public function installBudgetCalculator(array &$form, FormStateInterface $form_state) {
    // Remove configuration data.
    $this->configFactory->getEditable('layout_builder_browser.layout_builder_browser_block.budget_calculator')->delete();

    $this->module_installer->install(['uw_budget_calculator']);

    // Display a message on completion.
    $this->messenger->addStatus($this->t('The Budget Calculator module has been installed.'));
  }

  /**
   * Installs Admission Requirements module.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state array.
   */
  public function installAdmissionRequirements(array &$form, FormStateInterface $form_state) {
    // Remove configuration data.
    $this->configFactory->getEditable('layout_builder_browser.layout_builder_browser_block.uw_admission_requirements')->delete();

    $this->module_installer->install(['uw_admission_requirements']);

    // Display a message on completion.
    $this->messenger->addStatus($this->t('The Admission Requirements module has been installed.'));
  }

  /**
   * Clears drupal's cache.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state array.
   */
  public function clearCache(array &$form, FormStateInterface $form_state) {
    drupal_flush_all_caches();

    // Display a message on completion.
    $this->messenger->addStatus($this->t('Caches have been cleared.'));
  }

}
